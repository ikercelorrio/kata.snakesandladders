﻿using Kata.SnakesAndLadders.GameLib;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace Kata.SnakesAndLadders
{
    class Program
    {
        static void Main(string[] args)
        {
            IReadOnlyDictionary<int, int> squareLinks = TokenMovementNormalizer.DefaultSnakesAndLadder;
            var config = new GameConfig();
            var dice = new SixFacedDice();
            var diceWithLog = new DiceWithConsoleOutput(dice);
            var tokenManager = new TokenManager(config);
            var tomenMovementNormalizer = new TokenMovementNormalizer(squareLinks, config);

            var game = new Game(config, diceWithLog, tokenManager, tomenMovementNormalizer);

            squareLinks.PrintConsole();

            Console.WriteLine("Press a key to start the game.");
            Console.ReadKey();

            game.StartNew(2);
            Console.WriteLine($"Game started with {game.GetTokens().Count()} players.");

            while (!game.Ended)
            {
                var tokens = game.GetTokens();
                tokens.PrintConsole(game.GetPlayerTurn());
                game.PlayTurn();
            }

            var player = game.GetPlayerTurn();
            Console.WriteLine($"\n{new string('-', 80)}\n");
            Console.WriteLine($"\nPlayer #{player} win the game.\n");
            Console.WriteLine($"\n{new string('-', 80)}\n");
            Console.WriteLine("Press any key to exit.");
            Console.WriteLine("");
            Console.ReadKey();
        }
    }

    public static class PrintConsoleOutputExtensions
    {
        public static void PrintConsole(this IEnumerable<IToken> tokens, int playerTurn)
        {
            Func<int, int, string> tokenToString =
                (id, position) => $"\t#{id}\t{position,5}\t{id == playerTurn}";

            Console.WriteLine("Tokens:\n\tPlayer\tSquare\tTurn");
            Console.WriteLine(string.Join("\n", tokens.Select(token => tokenToString(token.Id, token.Position))));
            Console.WriteLine("");
            Console.WriteLine("Press a key to play turn.");
            Console.WriteLine("");
            Console.ReadKey();
        }

        public static void PrintConsole(this IReadOnlyDictionary<int, int> links)
        {
            Func<int, int, string> snakeOrLadderToString =
                (from, to) => from > to ? "snake" : "ladder";

            Func<int, int, string> linkToString =
                (from, to) => $" {snakeOrLadderToString(from, to),-6}: {from,2} -> {to,2}";

            Console.WriteLine($"\n{new string('-', 80)}\n");
            Console.WriteLine("Square Links:");
            Console.WriteLine(string.Join("\n", links.Select(link => linkToString(link.Key, link.Value))));
            Console.WriteLine($"\n{new string('-', 80)}\n");
        }
    }


    public class DiceWithConsoleOutput : IDice
    {
        private IDice _next;

        public DiceWithConsoleOutput(IDice next)
        {
            _next = next;
        }

        public int Roll()
        {
            int result = _next.Roll();
            Console.WriteLine($"Dice rolls and result {result}");
            return result;
        }
    }
}
