﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kata.SnakesAndLadders.GameLib
{
    public class TokenManager : ITokenManager
    {
        private readonly IGameConfig _config;

        private Dictionary<int, Token> _tokens = new Dictionary<int, Token>();

        public TokenManager(IGameConfig config)
        {
            _config = config;
        }

        public void Reset(int numberOfPlayers)
        {
            if (numberOfPlayers < _config.MinimunNumberOfPlayers ||
                numberOfPlayers > _config.MaximunNumberOfPlayers)
            {
                throw new ArgumentOutOfRangeException(nameof(numberOfPlayers));
            }

            _tokens = Enumerable
                .Range(1, numberOfPlayers)
                .Select(id =>
                new Token()
                {
                    Id = id,
                    Position = _config.StartSquare,
                })
                .ToDictionary(token => token.Id);
        }

        public Token GetToken(int playerId)
        {
            return _tokens[playerId];
        }

        public IEnumerable<Token> GetTokens()
        {
            return _tokens.Select(token => token.Value);
        }
    }
}