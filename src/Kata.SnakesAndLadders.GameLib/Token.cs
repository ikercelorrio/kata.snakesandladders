﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.SnakesAndLadders.GameLib
{
    public class Token : IToken
    {
        public int Id { get; set; }
        public int Position { get; set; }
    }
}
