﻿namespace Kata.SnakesAndLadders.GameLib
{
    public interface IGameConfig
    {
        int StartSquare { get; }
        int FinishSquare { get; }
        int MinimunNumberOfPlayers { get; }
        int MaximunNumberOfPlayers { get; }
    }
}