﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.SnakesAndLadders.GameLib
{
    public class Game
    {
        private readonly IGameConfig _config;
        private readonly IDice _dice;
        private readonly ITokenMovementNormalizer _tokenMovementNormalizer;
        private readonly ITokenManager _tokenManager;

        private int _numberOfPlayers;
        private int _turnCount;
        private bool _ended;

        public Game(
            IGameConfig config, 
            IDice dice,
            ITokenManager tokenManager,
            ITokenMovementNormalizer tokenMovementNormalizer)
        {
            _config = config;
            _dice = dice;
            _tokenManager = tokenManager;
            _tokenMovementNormalizer = tokenMovementNormalizer;
        }

        public void StartNew(int numberOfPlayers)
        {
            _turnCount = 0;
            _numberOfPlayers = numberOfPlayers;
            _tokenManager.Reset(numberOfPlayers);
            _ended = false;
        }

        public IEnumerable<IToken> GetTokens()
        {
            return _tokenManager.GetTokens();
        }

        public int GetPlayerTurn()
        {
            return (_turnCount % _numberOfPlayers) + 1;
        }

        public void PlayTurn()
        {
            if (_ended)
            {
                throw new GameEndedException("Cant move token because game already ended.");
            }

            int squaresToMove = _dice.Roll();
            var token = _tokenManager.GetToken(GetPlayerTurn());
            int tokenDestination = _tokenMovementNormalizer.NormalizeDestination(token.Position, squaresToMove);
            token.Position = tokenDestination;

            if (tokenDestination != _config.FinishSquare)
            {
                _turnCount++;
                return;
            }
            _ended = tokenDestination == _config.FinishSquare;
        }

        [Obsolete]
        public void Move(int squaresToMove)
        {
            var token = _tokenManager.GetToken(GetPlayerTurn());
            int tokenDestination = _tokenMovementNormalizer.NormalizeDestination(token.Position, squaresToMove);
            token.Position = tokenDestination;

            if (tokenDestination != _config.FinishSquare)
            {
                _turnCount++;
                return;
            }
            _ended = tokenDestination == _config.FinishSquare;
        }

        public bool Ended => _ended;
    }
}
