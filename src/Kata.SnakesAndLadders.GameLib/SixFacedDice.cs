﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.SnakesAndLadders.GameLib
{
    public class SixFacedDice : IDice
    {
        private Random _random = new Random();

        public int Roll()
        {
            return _random.Next(1, 7);
        }
    }
}
