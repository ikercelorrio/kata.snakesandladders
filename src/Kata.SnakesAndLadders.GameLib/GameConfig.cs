﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.SnakesAndLadders.GameLib
{
    public class GameConfig : IGameConfig
    {
        public int StartSquare { get; set; } = 1;
        public int FinishSquare { get; set; } = 100;

        public int MinimunNumberOfPlayers { get; set; } = 2;
        public int MaximunNumberOfPlayers { get; set; } = 4;
    }
}
