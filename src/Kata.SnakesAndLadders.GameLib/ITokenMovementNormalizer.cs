﻿using System.Collections.Generic;

namespace Kata.SnakesAndLadders.GameLib
{
    public interface ITokenMovementNormalizer
    {
        int NormalizeDestination(int tokenPosition, int squaresToMove);
    }
}
