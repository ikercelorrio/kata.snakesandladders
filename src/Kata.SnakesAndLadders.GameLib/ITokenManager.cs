﻿using System.Collections.Generic;

namespace Kata.SnakesAndLadders.GameLib
{
    public interface ITokenManager
    {
        void Reset(int numberOfPlayers);
        Token GetToken(int playerId);
        IEnumerable<Token> GetTokens();
    }
}