﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Kata.SnakesAndLadders.GameLib
{
    public class TokenMovementNormalizer : ITokenMovementNormalizer
    {
        private readonly IGameConfig _config;

        private IReadOnlyDictionary<int, int> _links;

        public static IReadOnlyDictionary<int, int> DefaultSnakesAndLadder => 
            new Dictionary<int, int>()
            {
                // ladders
                 {2,38},  {7,14},  {8,31}, {15,26}, {21,42},
                {28,84}, {36,44}, {51,67}, {79,98}, {71,91}, {87,94},

                // snakes
                 {16,6}, {46,25}, {49,11}, {62,19}, {64,60},
                {74,53}, {89,68}, {92,88}, {95,75}, {99,80},
            };


        public TokenMovementNormalizer(IReadOnlyDictionary<int, int> links, IGameConfig config)
        {
            _links = links ?? new Dictionary<int, int>();
            _config = config;
        }

        public int NormalizeDestination(int tokenPosition, int squaresToMove)
        {
            int calculatedDestination = squaresToMove + tokenPosition;
            int normalizedDestination = tokenPosition;
            if (calculatedDestination <= _config.FinishSquare)
            {
                if (_links.TryGetValue(calculatedDestination, out int linkDestination))
                {
                    normalizedDestination = linkDestination;
                }
                else
                {
                    normalizedDestination = calculatedDestination;
                }
            }

            return normalizedDestination;
        }
    }
}
