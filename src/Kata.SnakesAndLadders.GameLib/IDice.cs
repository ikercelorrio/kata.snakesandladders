﻿namespace Kata.SnakesAndLadders.GameLib
{
    public interface IDice
    {
        int Roll();
    }
}