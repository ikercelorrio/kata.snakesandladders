﻿namespace Kata.SnakesAndLadders.GameLib
{
    public interface IToken
    {
        int Id { get; }
        int Position { get; }
    }
}