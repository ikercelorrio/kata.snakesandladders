# Kata SnakesAndLadders

## Exercise

Snakes and Ladders is a board game involving two or more players rolling dice in order to move their tokens across a board. The board is made up of a collection of numbered squares and is adorned with 'snakes' and 'ladders', which link two squares on the board- snakes link the squares downwards whilst ladders link them going upwards. This means that landing at the bottom of a ladder moves you to the top of that ladder, whereas landing on the top of a snake moves you to the bottom of that snake. The objective of the game is to get your token to the final square before your opponents do.

From a technical point of view, the implementation of this game needs to be platform agnostic. We are going to want to launch this game on multiple devices and need a solid, robust game library which we can use as the backend for every frontend we stick on it. For this reason, we are not interested in the frontend you choose to use to test the game, only that the core game logic is separate and tested. 

![board](docs/img/snakesandladdersboard.jpg)

The first feature we want implemented is the ability to move your token across the board using dice rolls. Players will need the ability to roll a dice, move their token the number of squares indicated by the dice roll and should win if they land on the final square. The feature is splitted in three user stories.

## Requirements

- net 5.0 sdk or greater

## How To Run Console Demo 

1. Clone Repository  
   ```
    git clone git@gitlab.com:ikercelorrio/kata.snakesandladders.git
    cd kata.snakesandladders
    git switch -c main
   ```
1. Restore nuget packages 
   ```
   dotnet restore
   ```
1. Run Console Demo  
   ```
   dotnet run --project src/Kata.SnakesAndLadders
   ```

## How To Contribute

Follow the coding style found [here^](https://github.com/dotnet/runtime/blob/main/docs/coding-guidelines/coding-style.md)

1. Clone Repository  
   ```
   git clone Kata.SnakesAndLadders.git
   ```

1. Change working directory to the cloned repository directory
   ```
   cd Kata.SnakesAndLadders
   ```

1. Restore nuget packages 
   ```
   dotnet restore
   ```
1. Build the solution
   ```
   dotnet build
   ```
1. Run all the test
   ```
   dotnet test
   ```
1. Run the unit test
   ```
   dotnet test
   ```
1. To run specific test use the `--filter` option
   The User Stories Acceptance tests definitions [here](docs/userstories.md)
   |filter|description|
   |:-|:-|
   |UnitTest | Only Unit Tests|
   |AcceptanceTests | All User Stories Acceptance Tests|
   |US1 | Only User Story 1 Acceptance Tests|
   |US2 | Only User Story 2 Acceptance Tests|
   |US3 | Only User Story 3 Acceptance Tests|

