﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kata.SnakesAndLadders.GameLib;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using FluentAssertions;
using FluentAssertions.Execution;

namespace Kata.SnakesAndLadders.GameLib.UnitTests
{
    [TestClass()]
    public class GameTests
    {
        private static IToken CreateFakeToken(int id, int position)
        {
            return new Token()
            {
                Id = id,
                Position = position,
            };
        }

        [TestMethod()]
        public void StartNew_NumberOfPlayersBeBetween2And4_ShouldCreateSameNumberOfTokens()
        {
            // arrange
            var tokenManager = Substitute.For<ITokenManager>();
            var game =
                new GameBuilder()
                    .AddTokenManager(tokenManager)
                    .Build();

            // act
            game.StartNew(2);

            // assert
            tokenManager.Received(1).Reset(2);
        }

        [TestMethod()]
        public void StartNew_ShouldNotBeEnded()
        {
            // arrange
            var game =
                new GameBuilder()
                    .Build();

            // act
            game.StartNew(2);

            // assert
            game.Ended.Should().BeFalse();
        }

        [TestMethod()]
        public void StartNew_ThePlayerWhoHaveTurn_ShouldBe1()
        {
            // arrange
            var game =
                new GameBuilder()
                    .Build();

            // act
            game.StartNew(4);
            int playerWhoHaveTurn = game.GetPlayerTurn();

            // assert
            playerWhoHaveTurn.Should().Be(1);
        }

        [TestMethod()]
        public void StartNew_AndPlayer1PlayItsTurn_ShouldBeThePlayer2WhoHaveTurn()
        {
            // arrange
            int originSquare = 1;
            int squaresToMove = 2;
            int numberOfPlayers = 4;

            int expectedPlayerWhoHaveTurn = 2;

            var tokenManager = Substitute.For<ITokenManager>();
            tokenManager.GetToken(1).Returns(CreateFakeToken(id: 1, position: 1));

            var tokenMovementNormalizer = Substitute.For<ITokenMovementNormalizer>();
            tokenMovementNormalizer.NormalizeDestination(originSquare, squaresToMove)
                                   .Returns(originSquare + squaresToMove);

            var game =
                new GameBuilder()
                    .AddTokenManager(tokenManager)
                    .AddTokenMovementNormalizer(tokenMovementNormalizer)
                    .MockGameConfig(config => config.FinishSquare.Returns(100))
                    .MockDice(dice => dice.Roll().Returns(squaresToMove))
                    .Build();

            // act
            game.StartNew(numberOfPlayers);
            game.PlayTurn();
            int playerWhoHaveTurn = game.GetPlayerTurn();

            // assert
            using (new AssertionScope())
            {
                playerWhoHaveTurn.Should().Be(expectedPlayerWhoHaveTurn);

                tokenManager.Received(1).GetToken(1);
                tokenMovementNormalizer.Received(1).NormalizeDestination(originSquare, squaresToMove);
            }
        }

        [TestMethod]
        public void WhenTokenOn97AndMove3_TheGameShouldEnd()
        {
            // arrange
            int numberOfPlayers = 2;
            int squaresToMove = 3;
            int originSquare = 97;
            int finishSquare = originSquare + squaresToMove;

            int expectedPlayerWhoHaveTurn = 1;

            var tokenManager = Substitute.For<ITokenManager>();
            tokenManager.GetToken(1).Returns(CreateFakeToken(id: 1, position: 97));

            var tokenMovementNormalizer = Substitute.For<ITokenMovementNormalizer>();
            tokenMovementNormalizer.NormalizeDestination(originSquare, squaresToMove)
                                   .Returns(finishSquare);

            var game =
                new GameBuilder()
                    .AddTokenManager(tokenManager)
                    .AddTokenMovementNormalizer(tokenMovementNormalizer)
                    .MockGameConfig(config => config.FinishSquare.Returns(finishSquare))
                    .MockDice(dice => dice.Roll().Returns(squaresToMove))
                    .Build();

            // ""arrange""
            game.StartNew(numberOfPlayers);

            // act
            game.PlayTurn();
            int playerWhoHaveTurn = game.GetPlayerTurn();

            // assert
            using (new AssertionScope())
            {
                playerWhoHaveTurn.Should().Be(expectedPlayerWhoHaveTurn);
                game.Ended.Should().BeTrue();

                tokenManager.Received(1).GetToken(1);
                tokenMovementNormalizer.Received(1).NormalizeDestination(originSquare, squaresToMove);
            }
        }

        [TestMethod]
        public void WhenTokenOn97AndMove3AndMoveAgain_ShouldThrowAnGameEndedException()
        {
            // arrange
            int numberOfPlayers = 2;
            int squaresToMove = 3;
            int originSquare = 97;
            int finishSquare = originSquare + squaresToMove;

            var tokenManager = Substitute.For<ITokenManager>();
            tokenManager.GetToken(1).Returns(CreateFakeToken(id: 1, position: 97));
            tokenManager.GetToken(2).Returns(CreateFakeToken(id: 2, position: 1));

            var tokenMovementNormalizer = Substitute.For<ITokenMovementNormalizer>();
            tokenMovementNormalizer.NormalizeDestination(originSquare, squaresToMove)
                                   .Returns(finishSquare);

            var game =
                new GameBuilder()
                    .AddTokenManager(tokenManager)
                    .AddTokenMovementNormalizer(tokenMovementNormalizer)
                    .MockGameConfig(config => config.FinishSquare.Returns(finishSquare))
                    .MockDice(dice => dice.Roll().Returns(squaresToMove, squaresToMove))
                    .Build();

            // ""arrange""
            game.StartNew(numberOfPlayers);
            game.PlayTurn();
            Action actUnderTest = () => game.PlayTurn();

            // act + assert
            actUnderTest.Should().Throw<GameEndedException>(because: "after end game, could not try to move any token");
        }
    }

    public class GameBuilder
    {
        private IDice _dice = Substitute.For<IDice>();
        private IGameConfig _config = Substitute.For<IGameConfig>();
        private ITokenManager _tokenManager = Substitute.For<ITokenManager>();
        private ITokenMovementNormalizer _tokenMovementNormalizer = Substitute.For<ITokenMovementNormalizer>();

        public GameBuilder CreateIntegrationGameBuilder(IGameConfig config = null)
        {
            config = config ?? new GameConfig();
            return this
                    .AddGameConfig(config)
                    .AddTokenManager(
                        new TokenManagerBuilder()
                            .AddGameConfig(config)
                            .Build())
                    .AddTokenMovementNormalizer(
                        new TokenMovementNormalizerBuilder()
                            .AddGameConfig(config)
                            .Build());
        }

        public GameBuilder AddDice(IDice dice)
        {
            _dice = dice;
            return this;
        }

        public GameBuilder MockDice(Action<IDice> dice)
        {
            dice(_dice);
            return this;
        }

        public GameBuilder AddGameConfig(IGameConfig config)
        {
            _config = config;
            return this;
        }

        public GameBuilder MockGameConfig(Action<IGameConfig> config)
        {
            config(_config);
            return this;
        }

        public GameBuilder AddTokenManager(ITokenManager tokenManager)
        {
            _tokenManager = tokenManager;
            return this;
        }

        public GameBuilder AddTokenMovementNormalizer(ITokenMovementNormalizer tokenMovementNormalizer)
        {
            _tokenMovementNormalizer = tokenMovementNormalizer;
            return this;
        }

        public Game Build()
        {
            return new Game(
                _config,
                _dice,
                _tokenManager,
                _tokenMovementNormalizer);
        }
    }
}
