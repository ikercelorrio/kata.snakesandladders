﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kata.SnakesAndLadders.GameLib;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;

namespace Kata.SnakesAndLadders.GameLib.UnitTests
{
    [TestClass()]
    public class SixFacedDiceTests
    {
        [TestMethod()]
        public void SixFacedDiceRoll_ShouldResultBetween1And6()
        {
            // arrange
            var dice = new SixFacedDice();

            // act
            var squares = dice.Roll();

            //assert
            squares.Should().BeInRange(1, 6);
        }
    }
}