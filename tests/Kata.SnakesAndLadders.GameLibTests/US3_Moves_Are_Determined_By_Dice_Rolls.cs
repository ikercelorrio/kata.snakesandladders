﻿using FluentAssertions;
using FluentAssertions.Execution;
using NSubstitute;

using Kata.SnakesAndLadders.GameLib.UnitTests;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.SnakesAndLadders.GameLib.AcceptanceTests.US3
{
    // As a player
    // I want to move my token based on the roll of a die
    // So that there is an element of chance in the game
    [TestClass]
    public class US3_Moves_Are_Determined_By_Dice_Rolls
    {
        [TestMethod]
        public void US3_UAT1_WhenPlayerRollsTheDice_TheResultWhouldBeBeTween1And6()
        {
            // **UAT1**
            using (new AssertionScope())
            {
                // Given the game is started
                var dice = new SixFacedDice();

                // When the player rolls a die
                int diceRollResult = dice.Roll();

                // Then the result should be between 1-6 inclusive
                diceRollResult.Should().BeInRange(1, 6);
            }
        }

        [TestMethod]
        public void US3_UAT2_WhenPlayerMove4_TheTokenShouldBeMoved4Spaces()
        {
            // ** UAT2**
            using (new AssertionScope())
            {
                var game =
                    new GameBuilder()
                        .CreateIntegrationGameBuilder()
                        .MockDice(dice => dice.Roll().Returns(4))
                        .Build();

                game.StartNew(2);

                // Given the player rolls a 4

                // When they move their token
                int squareBeforeMove =
                    game.GetTokens()
                        .First(token => token.Id == 1)
                        .Position;

                game.PlayTurn();

                // Then the token should move 4 spaces
                int squareAfterMove =
                    game.GetTokens()
                        .First(token => token.Id == 1)
                        .Position;

                int movedSpaces = squareAfterMove - squareBeforeMove;

                movedSpaces.Should().Be(4);
            }
        }
    }
}
