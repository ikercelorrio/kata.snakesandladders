﻿using FluentAssertions;
using FluentAssertions.Execution;

using Kata.SnakesAndLadders.GameLib.UnitTests;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using NSubstitute;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.SnakesAndLadders.GameLib.AcceptanceTests.US1
{
    [TestClass]
    public class US1_Token_Can_Move_Across_the_Board
    {
        [TestMethod]
        public void US1_UAT1_WhenGameStart_TheTokenIsOnSquare1()
        {
            using (new AssertionScope())
            {
                // Given the game is started
                // When the token is placed on the board
                // Then the token is on square 1;
                var game =
                    new GameBuilder()
                        .CreateIntegrationGameBuilder()
                        .Build();

                game.StartNew(2);

                int squareBeforeMove =
                    game.GetTokens()
                        .First(token => token.Id == 1)
                        .Position;

                squareBeforeMove.Should().Be(1);
            }
        }

        [TestMethod]
        public void US1_UAT2_WhenTokenIsOnSquare1_AndIsMoved3Spaces_AfterMoveShouldBeOnSquare4()
        {
            using (new AssertionScope())
            {
                var game =
                    new GameBuilder()
                        .CreateIntegrationGameBuilder()
                        .MockDice(dice => dice.Roll().Returns(3))
                        .Build();

                game.StartNew(2);

                int squareBeforeMove =
                    game.GetTokens()
                        .First(token => token.Id == 1)
                        .Position;

                // Given the token is on square 1
                squareBeforeMove.Should().Be(1);

                // When the token is moved 3 spaces
                game.PlayTurn();

                // Then the token is on square 4
                int squareAfterMove4 =
                    game.GetTokens()
                        .First(token => token.Id == 1)
                        .Position;

                squareAfterMove4.Should().Be(4);
            }
        }


        [TestMethod]
        public void US1_UAT3_WhenTokenIsOnSquare1_AndIsMoved3Spaces_AndThenIsMoved4Spaces_AfterSecondMoveShouldBeOnSquare8()
        {
            using (new AssertionScope())
            {
                // Given the token is on square 1
                var game =
                    new GameBuilder()
                        .CreateIntegrationGameBuilder(new GameConfig() { MinimunNumberOfPlayers = 1 })
                        .MockDice(dice => dice.Roll().Returns(3, 4))
                        .Build();

                game.StartNew(1);

                int squareBeforeMove =
                  game.GetTokens()
                        .First(token => token.Id == 1)
                        .Position;

                squareBeforeMove.Should().Be(1);

                // When the token is moved 3 spaces
                game.PlayTurn();
                int squareAfterFirstMove =
                   game.GetTokens()
                         .First(token => token.Id == 1)
                         .Position;

                squareAfterFirstMove.Should().Be(4);

                // And then it is moved 4 spaces
                game.PlayTurn();

                // Then the token is on square 8
                int squareAfterSecondMove =
                    game.GetTokens()
                        .First(token => token.Id == 1)
                        .Position;

                squareAfterSecondMove.Should().Be(8);
            }
        }
    }
}
