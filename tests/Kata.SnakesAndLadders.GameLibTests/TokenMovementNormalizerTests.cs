﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kata.SnakesAndLadders.GameLib;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using FluentAssertions;

namespace Kata.SnakesAndLadders.GameLib.UnitTests
{
    [TestClass()]
    public class TokenMovementNormalizerTests
    {
        [TestMethod()]
        public void WhenNormalize_WhenPositionIs1AndMove3_ShouldMoveToSquare4()
        {
            // arrange
            var sut = 
                new TokenMovementNormalizerBuilder()
                    .MockGameConfig(config => config.FinishSquare.Returns(100))
                    .Build();

            // act
            var squareToMove = sut.NormalizeDestination(1, 3);

            // assert
            squareToMove.Should().Be(4);
        }

        [TestMethod()]
        public void WhenNormalize_WhenPositionGoAheadFinish_DoNotMovePosition()
        {
            // arrange
            var sut =
                new TokenMovementNormalizerBuilder()
                    .MockGameConfig(config => config.FinishSquare.Returns(100))
                    .Build();

            // act
            var squareToMove = sut.NormalizeDestination(97, 4);

            // assert
            squareToMove.Should().Be(97);
        }

        [TestMethod()]
        public void WhenNormalize_WhenPositionGoToFinish_MoveToFinishSquare()
        {
            // arrange
            var sut =
                new TokenMovementNormalizerBuilder()
                    .MockGameConfig(config => config.FinishSquare.Returns(100))
                    .Build();

            // act
            var squareToMove = sut.NormalizeDestination(96, 4);

            // assert
            squareToMove.Should().Be(100);
        }

        [DataTestMethod]
        [DataRow(2, 30, 1, 1)]
        [DataRow(7, 14, 1, 6)]
        [DataRow(16, 6, 11, 5)]
        public void WhenNormalize_WhenDestinationHaveALink_ShouldMoveToLinkDestination(
            int linkOrigin, int linkDestination, int originSquare, int spacesToMove)
        {
            // arrange
            var links = new Dictionary<int, int>() { { linkOrigin, linkDestination } };

            var sut =
                new TokenMovementNormalizerBuilder()
                    .MockGameConfig(config => config.FinishSquare.Returns(100))
                    .AddLinks(links)
                    .Build();

            // act
            var squareToMove = sut.NormalizeDestination(originSquare, spacesToMove);

            // assert
            squareToMove.Should().Be(linkDestination);
        }

    }

    public class TokenMovementNormalizerBuilder
    {
        private IGameConfig _config = Substitute.For<IGameConfig>();
        private IReadOnlyDictionary<int, int> _links = new Dictionary<int, int>();

        public TokenMovementNormalizerBuilder AddGameConfig(IGameConfig config)
        {
            _config = config;
            return this;
        }
        public TokenMovementNormalizerBuilder MockGameConfig(Action<IGameConfig> config)
        {
            config(_config);
            return this;
        }

        public TokenMovementNormalizerBuilder AddLinks(IReadOnlyDictionary<int, int> links)
        {
            _links = links;
            return this;
        }

        public TokenMovementNormalizer Build()
        {
            return new TokenMovementNormalizer(
                _links,
                _config);
        }
    }
}