﻿using FluentAssertions;
using FluentAssertions.Execution;
using NSubstitute;

using Kata.SnakesAndLadders.GameLib.UnitTests;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kata.SnakesAndLadders.GameLib.AcceptanceTests.US2
{
    /// As a player
    /// I want to be able to win the game
    /// So that I can gloat to everyone around
    [TestClass]
    public class US2_Player_Can_Win_the_Game
    {
        [TestMethod]
        public void US2_UAT1_WhenTokenIsOnSquare97_AndMove3Spaces_ThenTheTokenSouldBeOnSquare100_AndPlayerHasWonTheGame()
        {
            using (new AssertionScope())
            {
                //**UAT1**
                var game =
                    new GameBuilder()
                        .CreateIntegrationGameBuilder(new GameConfig() { StartSquare = 97 })
                        .MockDice(dice => dice.Roll().Returns(3))
                        .Build();

                game.StartNew(2);

                int squareBeforeMove =
                    game.GetTokens()
                        .First(token => token.Id == 1)
                        .Position;

                //    Given the token is on square 97
                squareBeforeMove.Should().Be(97);

                //    When the token is moved 3 spaces
                game.PlayTurn();

                //    Then the token is on square 100
                int squareAfterMove3 =
                    game.GetTokens()
                        .First(token => token.Id == 1)
                        .Position;

                squareAfterMove3.Should().Be(100);

                //    And the player has won the game            
                game.Ended.Should().BeTrue();

                int playerTurn = game.GetPlayerTurn();
                playerTurn.Should().Be(1);

                bool player1WonTheGame = (game.Ended && game.GetPlayerTurn() == 1);

                player1WonTheGame.Should().BeTrue();
            }
        }

        [TestMethod]
        public void US2_UAT2_WhenTokenIsOnSquare97_AndMove44Spaces_ThenTheTokenSouldBeOnSquare97_AndPlayerHasNotWonTheGame()
        {
            using (new AssertionScope())
            {
                //** UAT2**
                var game =
                    new GameBuilder()
                        .CreateIntegrationGameBuilder(new GameConfig() { StartSquare = 97 })
                        .MockDice(dice => dice.Roll().Returns(4))
                        .Build();

                game.StartNew(2);

                int squareBeforeMove =
                    game.GetTokens()
                        .First(token => token.Id == 1)
                        .Position;

                //    Given the token is on square 97
                squareBeforeMove.Should().Be(97);

                //    When the token is moved 4 spaces
                game.PlayTurn();

                //    Then the token is on square 97
                int squareAfterMove4 =
                    game.GetTokens()
                        .First(token => token.Id == 1)
                        .Position;

                squareAfterMove4.Should().Be(97);

                //    And the player has not won the game
                game.Ended.Should().BeFalse();

                int playerTurn = game.GetPlayerTurn();
                playerTurn.Should().NotBe(1);
                playerTurn.Should().Be(2);

                bool player1WonTheGame = (game.Ended && game.GetPlayerTurn() == 1);

                player1WonTheGame.Should().BeFalse();
            }
        }
    }
}
