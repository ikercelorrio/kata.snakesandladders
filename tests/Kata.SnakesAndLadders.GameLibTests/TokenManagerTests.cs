﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Kata.SnakesAndLadders.GameLib;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using FluentAssertions;
using FluentAssertions.Execution;

namespace Kata.SnakesAndLadders.GameLib.UnitTests
{
    [TestClass()]
    public class TokenManagerTests
    {
        [TestMethod()]
        public void WhenReset_WithNumberOfPlayersLowerThan2_ShouldThrowAnArgumentOutOfRangeException()
        {
            // arrange
            var tokenManger =
                new TokenManagerBuilder()
                    .MockGameConfig(config => config.MinimunNumberOfPlayers.Returns(2))
                    .Build();

            Action actUndertest = () => tokenManger.Reset(1);

            // act + assert
            actUndertest.Should().Throw<ArgumentOutOfRangeException>();
        }

        [TestMethod()]
        public void WhenReset_WithNumberOfPlayersGreaterThan4_ShouldThrowAnArgumentOutOfRangeException()
        {
            // arrange
            var sut =
                new TokenManagerBuilder()
                    .MockGameConfig(config => config.MaximunNumberOfPlayers.Returns(4))
                    .Build();

            Action actUndertest = () => sut.Reset(5);

            // act + assert
            actUndertest.Should().Throw<ArgumentOutOfRangeException>();
        }

        [DataTestMethod()]
        [DataRow(2)]
        [DataRow(3)]
        [DataRow(4)]
        public void WhenReset_WithNumberOfPlayersBetween2And4_ShouldCreateSameNumberOfTokens(int numberOfPlayers)
        {
            // arrange
            var sut =
                new TokenManagerBuilder()
                    .MockGameConfig(config => config.MinimunNumberOfPlayers.Returns(2))
                    .MockGameConfig(config => config.MaximunNumberOfPlayers.Returns(4))
                    .Build();
            int expectedTokenCount = numberOfPlayers;

            // act
            sut.Reset(numberOfPlayers);
            var tokens = sut.GetTokens();

            // assert
            tokens.Count().Should().Be(expectedTokenCount);
        }

        [DataTestMethod()]
        [DataRow(2, 1)]
        [DataRow(3, 2)]
        [DataRow(4, 3)]
        public void WhenReset_WithNumberOfPlayersBetween2And4_TokenPositionShouldBeTheStartSquare(int numberOfPlayers, int startSquare)
        {
            // arrange
            var sut =
                new TokenManagerBuilder()
                    .MockGameConfig(config => config.StartSquare.Returns(startSquare))
                    .MockGameConfig(config => config.MinimunNumberOfPlayers.Returns(2))
                    .MockGameConfig(config => config.MaximunNumberOfPlayers.Returns(4))
                    .Build();

            // act
            sut.Reset(numberOfPlayers);
            var tokens = sut.GetTokens();

            // assert
            using (new AssertionScope())
            {
                tokens.ToList().ForEach(token => token.Position.Should().Be(startSquare));
            }
        }

    }

    public class TokenManagerBuilder
    {
        private IGameConfig _config = Substitute.For<IGameConfig>();

        public TokenManagerBuilder AddGameConfig(IGameConfig config)
        {
            _config = config;
            return this;
        }

        public TokenManagerBuilder MockGameConfig(Action<IGameConfig> config)
        {
            config(_config);
            return this;
        }

        public TokenManager Build()
        {
            return new TokenManager(_config);
        }
    }
}
